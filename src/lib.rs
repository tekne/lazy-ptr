use erasable::{ErasablePtr, ErasedPtr};
use std::marker::PhantomData;
use std::ops::Deref;
use std::ptr::{null_mut, NonNull};
use std::sync::atomic::{self, AtomicPtr, Ordering::*};
use std::mem::ManuallyDrop;

/// A pointer type `P` which can be lazily initialized across multiple threads
pub struct OncePtr<P> {
    ptr: AtomicPtr<()>,
    phantom: PhantomData<P>,
}

impl<P> OncePtr<P> {
    /// Get the raw type-erased pointer to the payload, or `None` if the cell is empty.
    /// Enforces the atomic ordering passed
    pub fn load(&self, order: atomic::Ordering) -> Option<ErasedPtr> {
        let ptr = self.ptr.load(order);
        NonNull::new(ptr).map(ErasablePtr::erase)
    }
    /// Get the raw type-erased pointer to the payload, or `None` if the cell is empty.
    pub fn as_ptr(&self) -> Option<ErasedPtr> {
        self.load(Relaxed)
    }
    /// Check whether this cell is empty
    pub fn is_empty(&self) -> bool {
        self.load(Relaxed).is_none()
    }
    /// Attempt to set the underlying pointer of the cell to the given type-erased pointer.
    /// Returns `None` on success, and the pointer back on failure. Enforces the atomic ordering
    /// passed.
    ///
    /// # Safety
    /// It is undefined behaviour if the given pointer is not a valid instance of `P`, if `P`
    /// implements `ErasablePtr`.
    pub unsafe fn store_ptr(&self, ptr: ErasedPtr, order: atomic::Ordering) -> Option<ErasedPtr> {
        let raw: *mut () = ptr.cast().as_ptr();
        let result = self.ptr.compare_and_swap(null_mut(), raw, order);
        if result == null_mut() {
            None
        } else {
            Some(ptr)
        }
    }
    /// Attempt to set the underlying pointer of the cell to the given type-erased pointer.
    /// Returns `None` on success, and the pointer back on failure.
    ///
    /// # Safety
    /// It is undefined behaviour if the given pointer is not a valid instance of `P`, if `P`
    /// implements `ErasablePtr`.
    pub unsafe fn set_ptr(&self, ptr: ErasedPtr) -> Option<ErasedPtr> {
        self.store_ptr(ptr, Relaxed)
    }
}

impl<P: ErasablePtr> OncePtr<P> {
    /// Attempt to set the underlying pointer of the cell to the given pointer.
    /// Returns `None` on success, and the pointer back on failure.
    pub fn set(&self, ptr: P) -> Option<P> {
        unsafe {
            self.set_ptr(ErasablePtr::erase(ptr))
                .map(|ptr| ErasablePtr::unerase(ptr))
        }
    }
}
